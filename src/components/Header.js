import React from 'react';


const Header = () => (
    <div className={"header"}>
        <h1 className={"title"}>Zombie Apocalypse App</h1>
        <p className={"subtitle"}>Situation got pretty bad</p>
    </div>
);

export default Header;