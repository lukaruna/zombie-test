import React from "react";
import { compose, withProps, lifecycle } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";

//Now, I took most of this container from the web and adapted it to my need.

//Map container HOC
const MapContainer = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAv_oTO1iqm-2-ncqMIp8Ecqu0SW5EtrU8&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `400px` }} />,
        containerElement: <div style={{ height: `250px` }} />,
        mapElement: <div style={{ height: `250px` }} />,
    }),
    withScriptjs,
    withGoogleMap,


    )((props) =>

    <GoogleMap
        defaultZoom={14}
        center={{ lat: props.location[0], lng: props.location[1] }}
    >
        {props.isMarkerShown &&
        <Marker position={{ lat: props.location[0], lng: props.location[1] }}
                draggable ={true}
                title={"Drag to Location"}
                onDragEnd={(e) => props.handleDrag((e.latLng))}
        />}

    </GoogleMap>
)

//Map Container with Marks

class MapContainerWithMarks extends React.PureComponent {

    state = {
        isMarkerShown: true,
        };

    componentDidMount() {
        const refs = {}
        this.delayedShowMarker()
    };


    delayedShowMarker = () => {
        setTimeout(() => {
            this.setState({ isMarkerShown: true })
        }, 3000)
    };




    render() {
        return (
            <MapContainer
                isMarkerShown={this.state.isMarkerShown}
                location={[this.props.lat, this.props.lng]}
                handleDrag={this.props.handleMarkerLocation}
            />
        )
    }
}

module.exports= {MapContainerWithMarks};
