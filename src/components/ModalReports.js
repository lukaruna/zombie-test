import React, {Component} from 'react';

import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import Close from 'material-ui/svg-icons/navigation/close';
import Divider from 'material-ui/Divider';
import CircularProgress from 'material-ui/CircularProgress';

import axios from "axios/index";



export default class Reports extends Component {

    state = {

        //Fetches data from all the reports.

        infected: localStorage.getItem('infected'),

        pointsLost: localStorage.getItem('points_lost'),

        date: localStorage.getItem('dateReport'),
        //Items and averages
        totalItems: JSON.parse(localStorage.getItem('totalItems')),
        avgFood: JSON.parse(localStorage.getItem('avgFood')),
        avgWater: JSON.parse(localStorage.getItem('avgWater')),
        avgMedication: JSON.parse(localStorage.getItem('avgMedication')),
        avgAmmunition: JSON.parse(localStorage.getItem('avgAmmunition')),

        circularProgress: false

    }

    componentWillReceiveProps () {
        this.handleFetchData();

    }

    //Receives all data needed.
    handleFetchData = () => {
        //Updating list of survivors

        this.setState(() => ({circularProgress: true}))

        axios.get('http://zssn-backend-example.herokuapp.com/api/people.json')
            .then((response) => {


                const data = response.data;
                const date = new Date();
                localStorage.setItem('data', JSON.stringify(data));
                localStorage.setItem('dateReport', JSON.stringify(date));

                this.setState(() => ({
                    date: localStorage.getItem('dateReport')
                }))

                this.handleInfected();
                this.handlePointsLost();
                this.handleItems(data);

            })
            .catch((error) =>  {
                this.setState(() => ({circularProgress: false}))
                return `${error}`
            });


    };


    //Receives infected percentage. Fires inside handleFetchData.
    handleInfected = () => {


        // Updating percentage of infected and non-infected

        axios.get('http://zssn-backend-example.herokuapp.com/api/report/infected.json')
            .then((response) => {
                const infected = response.data.report.average_infected;
                localStorage.setItem('infected', JSON.stringify(response.data.report.average_infected));
                this.setState(() => ({infected: localStorage.getItem('infected')}));

            })
            .catch( error => `Couldn't fetch average!`)
    };



    //Points lost per infected
    handlePointsLost = () => {

        axios.get('http://zssn-backend-example.herokuapp.com/api/report/infected_points.json')
            .then((response) => {
                localStorage.setItem('pointsLost', JSON.stringify(response.data.report.total_points_lost));
                this.setState(() => ({
                    pointsLost: localStorage.getItem('pointsLost')
                }));
            })
            .catch(error => `Couldn't fetch total points!`);
    }


    //Fetching total Items registered
    handleItems = (data) => {

        let id;
        let totalItems = [0,0,0,0]; //Food, Water, Medication, Ammunition
        let ids = [];


        //Filtering infected fellas.
        const list = data.filter(cur => !cur["infected?"]);

        //Fetching ids
        ids = list.map((current) => current.location.replace('http://zssn-backend-example.herokuapp.com/api/people/', ''));

        //For each id, fetch one's items, sum it on totalItems and setState totalItems = response
        ids.forEach((cur, index) => {

            axios.get(`http://zssn-backend-example.herokuapp.com/api/people/${cur}/properties.json`)
                .then((response1) => {
                    let itemsPerSurvivor = [0,0,0,0];
                    response1.data.forEach((current) => {
                        current.item.name==="Food" ? itemsPerSurvivor[0] = itemsPerSurvivor[0] + current.quantity : undefined;
                        current.item.name==="Water" ? itemsPerSurvivor[1] = itemsPerSurvivor[1] + current.quantity : undefined;
                        current.item.name==="Medication" ? itemsPerSurvivor[2] = itemsPerSurvivor[2] + current.quantity : undefined;
                        current.item.name==="Ammunition" ? itemsPerSurvivor[3] = itemsPerSurvivor[3] + current.quantity : undefined;
                    });
                    return itemsPerSurvivor;
                }).then((response2) => {

                    totalItems[0] = totalItems[0]+response2[0];
                    totalItems[1] = totalItems[1]+response2[1];
                    totalItems[2] = totalItems[2]+response2[2];
                    totalItems[3] = totalItems[3]+response2[3];
                    return totalItems;
                }).then((response3) => {

                    const size = ids.length;
                    this.setState(() => ({
                        //totalItems
                        totalItems: response3,

                        //Now we calculate the averages!
                        avgFood: totalItems[0]/size,
                        avgWater: totalItems[1]/size,
                        avgMedication: totalItems[2]/size,
                        avgAmmunition: totalItems[3]/size,
                    }));


                    // In the last index, circuclarProgress is set to false.
                    if(index === ids.length-1){
                           this.setState(()=> ({
                                circularProgress: false
                            }))
                    }
                });

            //Put this data in localStorage
            localStorage.setItem('totalItems', JSON.stringify(this.state.totalItems));

            //Now the items
            localStorage.setItem('avgFood', JSON.stringify((this.state.avgFood)));
            localStorage.setItem('avgWater', JSON.stringify((this.state.avgWater)));
            localStorage.setItem('avgMedication', JSON.stringify((this.state.avgMedication)));
            localStorage.setItem('avgAmmunition', JSON.stringify((this.state.avgAmmunition)));

        });
        };



    render(){

        const mediumSize = {fontSize: 14}
        const smallSize = {fontSize: 12}

        return(
            <Dialog
                open={this.props.selected}
                onRequestClose={this.props.goBack}
                autoScrollBodyContent={true}
            >
                <br />

                <div className={"flex flex-row h-space--between"}>
                    <h3>Reports</h3>
                    <IconButton
                        onClick={this.props.goBack}
                        tooltip="Close"
                    >
                        <Close />
                    </IconButton>

                </div>
                <Divider />
                <br />
                {this.state.date===null
                    ? <p style={smallSize}>No data yet!</p>
                    : <p style={smallSize}>{`Last refresh was on: ${this.state.date}`}</p>}

                <RaisedButton
                    label={"Refresh Report"}
                    backgroundColor={"#ff9e40"}
                    onClick={this.props.selected? this.handleFetchData : undefined}
                    className={"but-margin-bottom"}
                />

                {this.state.circularProgress && <div>
                    <CircularProgress size ={60}/>
                </div>}

                {!this.state.circularProgress&&<div>
                    <p style={mediumSize}>{`Percentage of infected: ${this.state.infected? Math.floor(parseFloat(this.state.infected)*1000)/10+'%' : 'No data!'}`}</p>
                    <p style={mediumSize}>{`Percentage of non-infected: ${this.state.infected? Math.floor((1-parseFloat(this.state.infected))*1000)/10+'%' : 'No data!'}`}</p>
                    <p style={mediumSize}>{`Points lost in items that belong to infected people: ${this.state.pointsLost ? this.state.pointsLost +' points' : 'No data!'}`}</p>
                    <p style={mediumSize}>{`Average Food: ${this.state.avgFood>0 ? `${Math.floor(100*this.state.avgFood)/100} per survivor` : 'No data!'}`}</p>
                    <p style={mediumSize}>{`Average Water: ${this.state.avgWater>0 ?`${Math.floor(100*this.state.avgWater)/100} per survivor` : 'No data!'}`}</p>
                    <p style={mediumSize}>{`Average Medication: ${this.state.avgMedication>0 ? `${Math.floor(100*this.state.avgMedication)/100} per survivor` : 'No data!'}`}</p>
                    <p style={mediumSize}>{`Average Ammunition: ${this.state.avgAmmunition>0 ? `${Math.floor(100*this.state.avgAmmunition)/100} per survivor` : 'No data!'}`}</p>

                </div>}

            </Dialog>
        )
    }

}
