import React, {Component} from 'react';

import Dialog from 'material-ui/Dialog';
import AutoComplete from 'material-ui/AutoComplete';
import IconButton from 'material-ui/IconButton';
import Close from 'material-ui/svg-icons/navigation/close';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';

import axios from "axios/index";




// I couldn't make a transaction even in swagger API. Tried to print error.response on request,
// but didn't manage to find out what was wrong.

export default class ModalTradeItems extends Component {

    state = {
        open: false,
        names: JSON.parse(localStorage.getItem('names')),
    };



    handleName = (event, type) => {


        //Fills state name1 and name2.
        type === '0'
            ? this.setState(() => ({
                name1: event,
                snackbar: false,
                message: ''
            }))
            : this.setState(() => ({
                name2: event,
                snackbar: false,
                message: ''
            }))
    };



    handleFetchSurvivors = () => {

        this.setState(() => ({snackbar: false, message: ''}));



        axios.get('http://zssn-backend-example.herokuapp.com/api/people.json')
            .then((response) => {

                //Local Storage of food will update

                const data = response.data;
                const date = new Date();
                localStorage.setItem('data', JSON.stringify(data));
                localStorage.setItem('dateList', JSON.stringify(date));

                let names = data.map(cur => cur.name);

                localStorage.setItem('names', JSON.stringify(names));
                this.setState(() => ({
                    names
                }));

            })
            .catch((error) =>  ('Connection failture!'));


    };

    handleTrade = () => {

        const person_id = '77a4426b-8abb-416c-b148-a824927fd804';
        const name = 'Testador2';

        const consumer = {
            name,
            pick: 'Water:1',
            payment:'Food:1;Ammunition:1'
        };

        const obj = {
            person_id,
            consumer: consumer
        };
        console.log(obj.consumer.name);

        axios.post(`http://zssn-backend-example.herokuapp.com/api/people/${person_id}/properties/trade_item.json`, obj)
            .then((response) => console.log(response))
            .catch((error) => console.log(error.response))

    }


    render(){


        const margin={
            margin: '10%',
            border: '10%'
        };

        return(
            <Dialog
                open={this.props.selected}
                onRequestClose={this.props.goBack}
                contentLabel={"Trade Items"}
            >



                <div className={"flex flex-row h-space--between"}>
                    <h3>Trade items</h3>
                    <IconButton
                        onClick={this.props.goBack}
                        tooltip="Close"
                    >
                        <Close />
                    </IconButton>
                </div>

                <Divider />
                <RaisedButton
                    className={"but"}
                    label={"Refresh List"}
                    backgroundColor={"#ff9e40"}
                    onClick={this.handleFetchData}
                />

                <div>

                        <AutoComplete
                            style={{margin: 5}}
                            floatingLabelText="Your name"
                            dataSource={this.state.names ? this.state.names : []}
                            filter={AutoComplete.fuzzyFilter}
                            maxSearchResults={4}
                            onUpdateInput={(event) => this.handleName(event, '0')}
                        />

                        <AutoComplete
                            style={{margin: 5}}
                            floatingLabelText="Survivor's name"
                            dataSource={this.state.names ? this.state.names : []}
                            filter={AutoComplete.fuzzyFilter}
                            maxSearchResults={4}
                            onUpdateInput={(event) => this.handleName(event, '1')}
                        />
                </div>


                <RaisedButton
                    className={"but"}
                    label={"Trade"}
                    backgroundColor={"#ff9e40"}
                    onClick={this.handleTrade}
                />

            </Dialog>
        )
    }

}
