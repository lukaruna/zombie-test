import React, {Component} from 'react'

//MaterialUI Stuff
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import Snackbar from 'material-ui/Snackbar';
import Close from 'material-ui/svg-icons/navigation/close';
import IconButton from 'material-ui/IconButton';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import Divider from 'material-ui/Divider';

import {MapContainerWithMarks} from './MapContainer';


import axios from 'axios';



export default class ModalAddSurvivor extends Component {

    state = {

        //Snackbar state
        snackbar: false,
        message: '',

        //Person state
        name: '',
        gender: 'M',
        age: 0,
        id: undefined,

        //items, default set to 0
        water: 0,
        food: 0,
        ammunition: 0,
        medication: 0,

        lat: navigator.geolocation.getCurrentPosition(
            (response)=>this.setState(()=>({lat: response.coords.latitude})),
            (error)=>this.setState(()=>({lat: undefined}))
        ),

        lng: navigator.geolocation.getCurrentPosition(
            (response)=>this.setState(()=>({lng: response.coords.longitude})),
            (error)=>this.setState(()=>({lng: undefined}))
        ),




        //Modal state
        // 0: Interface for adding survivor name/gender/items
        // 1: Interface for adding survivor location
        // 2: Confirming data

        modalState: 0
    };


    //After button was clicked, this handler will fire and will send
    //validate data, send it to api, and fire a message via snackbar
    handleAddSurvivor = (e) => {

        e.preventDefault();

        //Field validation for empty takes.
        if(this.state.name && this.state.age && this.state.modalState === 0) {

            const items = `Water:${this.state.water};Food:${this.state.water};Medication:${this.state.medication};Ammunition:${this.state.ammunition}`;

            const obj = {
                name: this.state.name,
                age: this.state.age,
                gender: this.state.gender,
                items
            };


            //Sending it to API once data is validated
            axios.post('http://zssn-backend-example.herokuapp.com/api/people.json', obj).then((response) => {

                this.setState(() => ({
                    snackbar: true,
                    modalState: 1,
                    message: `${this.state.name} added!`,
                    id: response.data.id
                }))
            }).catch((error) => {
                if(error.response){
                    this.setState(() => ({
                        snackbar: true,
                        message: `${obj.name} already taken!`
                    }));
                } else {
                    this.setState(() => ({
                        snackbar: true,
                        message: `${error}`
                    }));
                }
            });

        //In case there are fields missing
        } else {
            this.setState(() => ({
                snackbar: true,
                message: 'There are fields missing!'
                }));
        }


    };

    //Validation for name fields, using regex
    handleName = (event) => {
        const name = event.target.value;

        //Regex pattern for only letters and space. Numbers and special characters are not allowed.
        const regex = new RegExp("^[a-zA-Z \b]+$");

        regex.test(name)||name.length===0
            ? this.setState(() => ({
                name,
                snackbar: false,
                message: ''
            }))
            : this.setState(() => ({
                snackbar: true,
                message: 'Must contain only letters and space!'
            }));
    };

    //Gender handler. Will fire when gender buttons get clicked.
    handleGender = (event) => {
        const gender = event.target.value;
        this.setState(() => ({gender}));
    };



    //Options for either skipping location or sending it.
    handleLocation = (message) => {

        switch (message){
            case "empty":
                this.setState(() => ({
                    snackbar:true,
                    message: `${this.state.name} location skipped!`,
                    modalState: 2
                }));

                break;



            case "location":
                this.setState(() => ({
                    snackbar:true,
                    message: `${this.state.name} location sent!`,
                    modalState: 2
                }));

                this.updateLocation();

                break;
        };
    };


    //Sends request to API. Gets survivor at this.state, and coordinates.
    //Make a few calls, snackbar error if needed.
    updateLocation = () => {

        const survivor = {
            name: this.state.name,
            age: this.state.age,
            gender: this.state.gender,
            id: this.state.id,
            lat: this.state.lat,
            lng: this.state.lng
        }

        axios.patch(`http://zssn-backend-example.herokuapp.com/api/people/${survivor.id}.json`, survivor)
            .then(response => {
                this.setState(() => ({
                    snackbar: true
                }))
            })
            .catch(error => this.setState(() => ({
                snackbar:true,
                message: error
            })));

    };

    //Method for closing and reseting states of this Modal. Will setState to go back to Modal 0,
    //with no remaining states.
    handleClose = () => {

        this.props.goBack();

        this.setState(() => ({
            name: '',
            gender: 'M',
            age: 0,
            water: 0,
            food: 0,
            ammunition: 0,
            water: 0,
            modalState: 0,
            snackbar: false,
            message: ''
        }));

    }

    //Field validation for all items. They're pretty much the same!
    //Only greater or equal to 0. Also, removes leading zeroes.
    handleItemAndAge = (event, type) => {

        // Regex removing leading zeroes
        const item = event.target.value.replace(/^0+(?=\d)/, '');

        switch (type){
            case 'water':
                item >= 0 ? this.setState(() => ({water: item})) : this.setState(() => ({water: 0}));
                break;
            case 'food':
                item >= 0 ? this.setState(() => ({food: item})) : this.setState(() => ({food: 0}));
                break;
            case 'medication':
                item >= 0 ? this.setState(() => ({medication: item})) : this.setState(() => ({medication: 0}));
                break;
            case 'ammunition':
                item >= 0 ? this.setState(() => ({ammunition: item})) : this.setState(() => ({ammunition: 0}));
                break;
            case 'age':
                item >= 0 ? this.setState(() => ({age: item})) : this.setState(() => ({age: 0}));
                break;
        }

        this.setState(()=> ({
            snackbar: false,
            message: ''
        }));
    };




    // This is the marker that will pass as props all the way
    // to GoogleMap (MapContainerWithMarks -> MapContainer -> GoogleMap).
    // It is responsible for fetching Marker's latitude and longitude.
    handleMarkerLocation = (latLng) => {
        this.setState(() => ({
            snackbar: false,
            lat: latLng.lat(),
            lng: latLng.lng()
        }));
    }


    render() {

        //Inline style margin for the text fields
        const style = {margin: 5}


        return(

            <div>

                {/*First Modal: name, age, gender and items!*/}

                {this.state.modalState === 0 && <Dialog
                    open={this.props.selected}
                    onRequestClose={this.handleClose}
                    autoScrollBodyContent={true}
                >
                    <div className={"flex flex-row h-space--between"}>
                        <h3>Add survivor</h3>
                        <IconButton
                            onClick={this.handleClose}
                            tooltip="Close"
                        >
                            <Close />
                        </IconButton>
                    </div>
                    <Divider />

                        {/*Name and age fields*/}

                        <TextField
                            floatingLabelText="Survivor name"
                            style={style}
                            type={"text"}
                            onChange={this.handleName}
                            value={this.state.name}
                        />

                    <TextField
                        style={style}
                        floatingLabelText="Age"
                        type={"number"}
                        value={this.state.age}
                        onChange={(event) => this.handleItemAndAge(event, 'age')}
                    />


                    {/*Radio Buttom for gender selection. Default is set to male on state*/}

                        <RadioButtonGroup
                            name="Select gender"
                            label={"Gender"}
                            onChange={this.handleGender}
                            defaultSelected={this.state.gender}
                        >
                            <RadioButton
                                value="M"
                                label="Male"
                            />
                            <RadioButton
                                value="F"
                                label="Female"
                            />
                        </RadioButtonGroup>

                        {/*Now all the 4 items fields*/}
                        <div>
                            <TextField
                                style={style}
                                floatingLabelText="Water supply"
                                type={"number"}
                                value={this.state.water}
                                onChange={(event) => this.handleItemAndAge(event, 'water')}
                            />
                            <TextField
                                style={style}
                                floatingLabelText="Food supply"
                                type={"number"}
                                value={this.state.food}
                                onChange={(event) => this.handleItemAndAge(event, 'food')}
                            />

                        </div>
                        <div>
                            <TextField
                                style={style}
                                floatingLabelText="Medication supply"
                                type={"number"}
                                value={this.state.medication}
                                onChange={(event) => this.handleItemAndAge(event, 'medication')}
                            />
                            <TextField
                                style={style}
                                floatingLabelText="Ammunition supply"
                                type={"number"}
                                value={this.state.ammunition}
                                onChange={(event) => this.handleItemAndAge(event, 'ammunition')}
                            />

                    </div>

                    {/*Add survivor button*/}
                    <RaisedButton
                        label={"Add survivor"}
                        backgroundColor={"#ff9e40"}
                        onClick={this.handleAddSurvivor}
                    />

                </Dialog>}


                {/*Modal 2: Fetches and sends/skips last location!
                   Will disable button "Send" if can't fetch location.
                */}

                {this.state.modalState === 1 && <Dialog
                    open={this.props.selected}
                    onRequestClose={this.handleClose}
                    autoScrollBodyContent={true}
                >

                    <div className={"flex flex-row h-space--between"}>
                        <h3>Last Location</h3>
                        <IconButton
                            onClick={this.handleClose}
                            tooltip="Close"
                        >
                            <Close />
                        </IconButton>
                    </div>

                    {this.state.lat&&<div>
                        <Divider />
                        <h6>Drag Marker to update location</h6>
                        <MapContainerWithMarks
                            lat={this.state.lat}
                            lng={this.state.lng}
                            handleMarkerLocation={this.handleMarkerLocation}
                        />
                    </div>

                    }

                    {!this.state.lat&&<div>
                        <Divider/>
                        <p>Needs your location.</p>
                    </div>
                    }


                    {/*Options: Send it or skip it!*/}
                    <div className={"flex flex-row h-center h-space--between"}>

                            <RaisedButton
                                className={"but"}
                                label={"Skip"}
                                backgroundColor={"#ff9e40"}
                                onClick={() => this.handleLocation("empty")}
                            />

                            <RaisedButton
                                className={"but"}
                                label={"Send"}
                                disabled={!this.state.lat}
                                backgroundColor={"#ff9e40"}
                                onClick={() => this.handleLocation("location")}
                            />

                    </div>


                </Dialog>}


                {/*Modal 3: We did it with success!*/}

                {this.state.modalState === 2 && <Dialog
                    open={this.props.selected}
                    onRequestClose={this.handleClose}
                    autoScrollBodyContent={true}
                >
                    <h2>{this.state.name} added with success!</h2>

                    <RaisedButton
                        label={"Ok!"}
                        backgroundColor={"#ff9e40"}
                        onClick={this.handleClose}
                    />

                </Dialog>}
                <div>

                    <Snackbar
                        open={this.state.snackbar}
                        message={this.state.message}
                        autoHideDuration={3000}
                    />

                </div>

            </div>

        )
    }

}
