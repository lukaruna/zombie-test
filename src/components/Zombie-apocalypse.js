import React from 'react';
import Header from './Header.js'
import Options from './Options.js'
import MuiThemeProvider from '../../node_modules/material-ui/styles/MuiThemeProvider.js';


//Root App.

const ZombieApocalypse = () => (
    <div>
            <Header />
            <MuiThemeProvider>
                <Options />
            </MuiThemeProvider>

    </div>
);


export default ZombieApocalypse;



