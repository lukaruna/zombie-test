import React, {Component} from 'react';

//MaterialUI stuff
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import AutoComplete from 'material-ui/AutoComplete';
import Close from 'material-ui/svg-icons/navigation/close';
import IconButton from 'material-ui/IconButton';
import Snackbar from 'material-ui/Snackbar';
import Divider from 'material-ui/Divider';

//Axios
import axios from "axios/index";

export default class ModalReportInfected extends Component {

    state = {

        names: JSON.parse(localStorage.getItem('names')),

        name1: '',
        name2: '',

        snackbar: false,
        message: '',

    };

    //Fetchs data before Mounting!
    componentWillMount = () => {
        this.handleFetchData();
    }


    //Fills name states when both fields change.
    handleName = (event, type) => {

        type === '0'
            ? this.setState(() => ({
                name1: event,
                snackbar: false,
                message: ''
            }))
            : this.setState(() => ({
                name2: event,
                snackbar: false,
                message: ''
            }))
    };


    //Fetches data to feed the Autocomplete dataSource
    handleFetchData = () => {

        this.setState(() => ({snackbar: false, message: ''}));

        axios.get('http://zssn-backend-example.herokuapp.com/api/people.json')
            .then((response) => {

                //Local Storage of food will update

                const data = response.data;
                const date = new Date();
                localStorage.setItem('data', JSON.stringify(data));
                localStorage.setItem('dateList', JSON.stringify(date));

                let names = data.map(cur => cur.name);

                localStorage.setItem('names', JSON.stringify(names));
                this.setState(() => ({
                    names
                }));

            })
            .catch((error) =>  ('Connection failture!'));


    };


    //Fires when report button gets clicked.
    //Will try to send the report, and throw snackbar errors if needed.
    //Error handling on names and requests are done here.
    handleReport = () => {

        let index1 = -1;
        let index2 = -1;
        const data = JSON.parse(localStorage.getItem('data'));


        //Checks if names are in the list.
        // Will fill index1 if fired on 1st field, or inde2 if fired on 2nd field

        data.forEach((cur, index) => {
            if (cur.name === this.state.name1) {
                index1 = index;
            }

            if (cur.name === this.state.name2) {
                index2 = index;
            }
        });


        //Both are in the list. Good, make the report!

        if (index1 != -1 && index2 != -1) {
            const req = {
                id: data[index1].location.replace('http://zssn-backend-example.herokuapp.com/api/people/',''),
                infected: data[index2].location.replace('http://zssn-backend-example.herokuapp.com/api/people/',''),
            };

            axios.post(`http://zssn-backend-example.herokuapp.com/api/people/${req.id}/report_infection.json`, req)
                .then((response) =>
                    this.setState(() => ({
                        snackbar: true,
                        message: `${this.state.name2} reported!`

                    }))
                )
                .catch((error) => {
                    if (error.response.status===422){
                        this.setState(() => ({
                            snackbar: true,
                            message: `${this.state.name1} cannot report ${this.state.name2} twice!`
                        }));
                    }
                    if (error.response.code===406){
                        this.setState(() => ({
                            snackbar: true,
                            message: `Not acceptable!`
                        }));
                    }
                });

        }


        //1st name isn't in the list. Snackbar msg: Error
        (index1 === -1 && index2!= -1)
            ? this.setState(() => ({
                snackbar: true,
                message: `${this.state.name1} isn't in the list!`
            })) : undefined;


        //2st name isn't in the list. Snackbar msg: Error
        (index2 === -1 && index1 != -1)
            ? this.setState(() => ({
                snackbar: true,
                message: `${this.state.name2} isn't in the list!`
            })) : undefined;

        //Both names aren't in the list! If they're empty, next setStates will take care of that
        (index1===-1 && index2===-1)
            ? this.setState(() => ({
                snackbar: true,
                message: `${this.state.name1} and ${this.state.name2} aren't in the list!`
            })): undefined;

        //Case 1st name isn't filled
        !this.state.name1
            ? this.setState(() => ({
                snackbar: true,
                message: 'First name not filled!'
        })) : undefined;

        //Case 2nd name isn't filled
        !this.state.name2
            ? this.setState(() => ({
                snackbar: true,
                message: 'Second name not filled!'
            })) : undefined;

        //Both names aren't filled!

        !this.state.name1&&!this.state.name2
            ? this.setState(() => ({
                snackbar: true,
                message: 'Both names are not filled!'
            })) : undefined;

    };


    //Closes the modal with no remaining state
    handleClose = () => {

        this.setState(() => ({
            name1: '',
            name2: '',
            snackbar: false,
            message: ''
        }));
        this.props.goBack();
    };

    render(){

        return(
            <Dialog
                open={this.props.selected}
                onRequestClose={this.handleClose}
            >

                <div className={"flex flex-row h-space--between"}>
                    <h3>Report an infected</h3>

                    <IconButton
                        onClick={this.handleClose}
                        tooltip="Close"
                    >
                        <Close />
                    </IconButton>

                </div>

                <Divider />
                <br />

                <RaisedButton
                    label={"Refresh List"}
                    backgroundColor={"#ff9e40"}
                    onClick={this.handleFetchData}
                />

                <p style={{fontSize: 12}}> {`Last Update: ${localStorage.getItem('dateList') ? localStorage.getItem('dateList') : 'never'}`}</p>

                <div>
                    <AutoComplete
                        style={{marginRight: 5}}
                        floatingLabelText="Your name"
                        dataSource={this.state.names ? this.state.names : []}
                        filter={AutoComplete.fuzzyFilter}
                        maxSearchResults={4}
                        onUpdateInput={(event) => this.handleName(event, '0')}
                    />
                    <AutoComplete
                        style={{marginLeft: 5}}
                        floatingLabelText="Infected name"
                        dataSource={this.state.names ? this.state.names : []}
                        filter={AutoComplete.fuzzyFilter}
                        maxSearchResults={4}
                        onUpdateInput={(event) => this.handleName(event, '1')}
                    />
                </div>

                <Snackbar
                    open={(this.state.snackbar)}
                    message={this.state.message}
                    autoHideDuration={3000}
                />

                <RaisedButton
                    label={"Report infected"}
                    backgroundColor={"#ff9e40"}
                    onClick={this.handleReport}
                    className={"but"}
                />

            </Dialog>
        )
    }

}
