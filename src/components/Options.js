import React, {Component} from 'react'
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import axios from 'axios';

//Icons
import MyLocation from 'material-ui/svg-icons/maps/add-location';
import PersonAdd from 'material-ui/svg-icons/social/person-add';
import CompareArrows from 'material-ui/svg-icons/action/compare-arrows';
import ReportProblem from 'material-ui/svg-icons/action/report-problem';
import AccountBalance from 'material-ui/svg-icons/action/account-balance';

//Modals
import ModalAddSurvivor from './ModalAddSurvivor';
import ModalUpdateLocation from './ModalUpdateLocation';
import ModalReportInfected from './ModalReportInfected';
import ModalTradeItems from './ModalTradeItems';
import ModalReports from './ModalReports';


export default class Options extends Component {

    //selectedOption. Will assist on opening and closing specific Modals

    state = {
        selectedOption: undefined
    };


    //handle different options for dialog opening.
    handleOption = (selectedOption) => (
       this.setState(() => ({selectedOption}))
    );

   // handleGoBack is passed as props, and will be called to close any open Modal.
    handleGoBack = () => {
        this.setState(() => ({selectedOption: undefined}))
    };



    render(){
        return (
            <div className={"flex v-center"}>
                {/*Options for interaction. onClick, will open specific Modal.*/}
                <BottomNavigation
                    style={{
                        height: 'auto !important',
                        width: '35rem',
                        display: 'flex',
                        justifyContent: 'center',
                        flexWrap: 'wrap',
                        flexDirection: 'column'
                    }}
                >
                    <div className={"flex v-center h-center"}
                         style={{
                             flexDirection: 'row',
                             flexWrap: 'wrap'
                    }}
                    >
                        <BottomNavigationItem
                            label="Add a survivor"
                            icon={<PersonAdd />}
                            onClick={() => this.handleOption('Add a survivor')}
                        />
                        <BottomNavigationItem
                            label="Update my last Location"
                            icon={<MyLocation/>}
                            onClick={() => this.handleOption('Update my last Location')}
                        />
                        <BottomNavigationItem
                            label="Report an infected"
                            icon={<ReportProblem/>}
                            onClick={() => this.handleOption('Report an infected')}
                        />
                        <BottomNavigationItem
                            label="Trade items"
                            icon={<CompareArrows/>}
                            onClick={() => this.handleOption('Trade items')}
                        />
                    </div>
                    <div className={"flex h-center v-center"}>
                        <BottomNavigationItem
                            label="Reports"
                            icon={<AccountBalance />}
                            onClick={() => this.handleOption('Reports')}
                        />
                    </div>
                </BottomNavigation>




                {/*Modals section. Will mount when bottom navigation items */}

                <ModalAddSurvivor
                    selected={this.state.selectedOption==='Add a survivor'}
                    goBack={this.handleGoBack}
                />

                <ModalUpdateLocation
                    selected={this.state.selectedOption==='Update my last Location'}
                    goBack={this.handleGoBack}
                />


                <ModalReportInfected
                    selected={this.state.selectedOption==='Report an infected'}
                    goBack={this.handleGoBack}
                />

                <ModalTradeItems
                    selected={this.state.selectedOption==='Trade items'}
                    goBack={this.handleGoBack}
                />

                <ModalReports
                    selected={this.state.selectedOption==='Reports'}
                    goBack={this.handleGoBack}
                    data = {this.handleFetchData}
                />

            </div>

        )
    }
}