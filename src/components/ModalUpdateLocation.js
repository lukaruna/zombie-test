import React, {Component} from 'react'
import axios from "axios/index";

import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import Close from 'material-ui/svg-icons/navigation/close';
import Refresh from 'material-ui/svg-icons/navigation/refresh';
import IconButton from 'material-ui/IconButton';
import Divider from 'material-ui/Divider';
import AutoComplete from "material-ui/AutoComplete/index";
import Snackbar from 'material-ui/Snackbar';

import { MapContainerWithMarks } from './MapContainer';



export default class ModalUpdateLocation extends Component {


    state={
        //Initial names come from localStorage. Refresh List button refreshes that list
        names: JSON.parse(localStorage.getItem('names')),


        //Lat and lng  based on geolocation

        lat: navigator.geolocation.getCurrentPosition(
                (response)=>this.setState(()=>({lat: response.coords.latitude})),
                (error)=>this.setState(()=>({lat: undefined}))
            ),

        lng: navigator.geolocation.getCurrentPosition(
                (response)=>this.setState(()=>({lng: response.coords.longitude})),
                (error)=>this.setState(()=>({lng: undefined}))
        ),

        snackbar: false,
        message: ''
    };


    componentWillReceiveProps(){
        this.handleRefresh();
    }


    //Will refresh both location and list
    handleRefresh = () => {
        this.getLocation();
        this.getData();
    }

    //Fires inside handleRefresh. Gets data and set it to localStorage
    getData = () => {

        axios.get('http://zssn-backend-example.herokuapp.com/api/people.json')
            .then((response) => {

                //Local Storage of food will update

                const data = response.data;
                const date = new Date();
                localStorage.setItem('data', JSON.stringify(data));
                localStorage.setItem('dateList', JSON.stringify(date));

                let names = data.map(cur => cur.name);

                localStorage.setItem('names', JSON.stringify(names));

                this.setState(() => ({
                    names
                }));

            })
    };

    //Fires inside handleRefresh.
       getLocation = () => {
        navigator.geolocation.getCurrentPosition(
            (response)=>(
                this.setState(()=>({
                    lat: response.coords.latitude,
                    lng: response.coords.longitude
                }))),
            (error)=>this.setState(()=>({lat: undefined, lng: undefined})));
    };


    //Update name state when updating autocomplete field
    handleName = (event) => {
        this.setState(() => ({
            name: event,
            snackbar: false
        }))
    }

    //This will send the patch request. Fires when Send button gets clicked
    updateLocation = () => {

        //Will update data when location gets fired
        this.getData();
        //Fetching survivors.
        //In case infected cannot updateLocation, add filter ".filter((cur) => !cur["infected?"]"
        const survivors = JSON.parse(localStorage.getItem('data'));


        //Fetching selected and id
        const selectedSurvivor = survivors.filter(
            (selected) => selected.name=== this.state.name ? true : false
        )[0];


        //If survivor is in the list
        if(selectedSurvivor){
            const id = selectedSurvivor.location.replace('http://zssn-backend-example.herokuapp.com/api/people/','');

            const survivor = {
                name: selectedSurvivor.name,
                gender: selectedSurvivor.gender,
                age: selectedSurvivor.age,
                lonlat: `POINT (${this.state.lat} ${this.state.lng})`
            };

            axios.patch(`http://zssn-backend-example.herokuapp.com/api/people/${id}.json`, survivor)
                .then(response => {
                    this.setState(() => ({
                        snackbar: true,
                        message: `${survivor.name} updated!`
                    }))
                })
                .catch(error => this.setState(() => ({
                    snackbar:true,
                    message: error
                })));

        }
        //If survivor isn't in the list
        else {
            this.setState(() => ({
                snackbar: true,
                message: 'Survivor not found!'
            }))
        }

    };



    //Closes the dialog with no remaining states
    handleClose = () => {
        this.props.goBack();
        this.setState(() => ({
            snackbar: false,
            message: ''
        }))
    }


    // This is the marker that will pass as props all the way
    // to GoogleMap (MapContainerWithMarks -> MapContainer -> GoogleMap).
    // It is responsible for fetching Marker's latitude and longitude.

    handleMarkerLocation = (latLng) => {
        this.setState(() => ({
            lat: latLng.lat(),
            lng: latLng.lng()
        }));
    }



    render(){
        return(
            <div>

                <Dialog
                    repositionOnUpdate={true}
                    open={this.props.selected}
                    onRequestClose={this.handleClose}
                    autoScrollBodyContent={true}
                >



                    <div className={"flex flex-row h-space--between"}>
                        <h3>Last Location</h3>

                        <IconButton
                            onClick={this.handleClose}
                            tooltip="Close"
                        >
                            <Close />
                        </IconButton>

                    </div>
                    <Divider/>
                    <div className={"flex flex-row v-center h-flex-right"}>

                        <AutoComplete
                            floatingLabelText="Your name"
                            textFieldStyle={{width:'50'}}
                            dataSource={this.state.names ? this.state.names : []}
                            filter={AutoComplete.fuzzyFilter}
                            maxSearchResults={4}
                            onUpdateInput={(event) => this.handleName(event)}
                        />

                        <IconButton
                            onClick={this.handleRefresh}
                            tooltip="Refresh Location and List"
                        >
                            <Refresh />
                        </IconButton>
                        <h6 className={"flex flex-rox h-flex-right"}>Drag Mark for repositioning!</h6>
                    </div>

                    {/*Map will only render if position was fetched!*/}
                    {this.state.lat&&<MapContainerWithMarks
                        lat={this.state.lat}
                        lng={this.state.lng}
                        handleMarkerLocation={this.handleMarkerLocation}
                    />}

                    {!this.state.lat&& <div>
                        Need your GPS location!
                    </div>}

                    <div>

                                <RaisedButton
                                    className={"but align-right"}
                                    label={"Send"}
                                    disabled={!this.state.lat}
                                    backgroundColor={"#ff9e40"}
                                    onClick={this.updateLocation}
                                />

                    </div>

                    <Snackbar
                        open={(this.state.snackbar)}
                        message={this.state.message}
                        autoHideDuration={3000}
                    />
                </Dialog>

            </div>

        )
    }

}
