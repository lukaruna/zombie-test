import React from 'react';
import ReactDOM from 'react-dom';
import ZombieApocalypse from './components/Zombie-apocalypse.js';
import './styles/styles.scss';
import 'normalize.css/normalize.css';


//Root application rendering!

ReactDOM.render(<ZombieApocalypse />, document.getElementById('app'));
